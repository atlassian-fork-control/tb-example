from __future__ import print_function

from tb import Tb


def echo_info(app: Tb, name: str, repo: str):
    app.term.info(f"Echo name: {name} repo: {repo}")
